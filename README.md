# README 

This README outlines important details for our major project
### What is this repository for? 

Our project, Brain Tour, aims to help people better understand the brain in a way that is simple yet detailed.

### How do I get set up? 

1. Download and install unity with MSVS Community or other code editor from (https://unity3d.com/get-unity/download)
2. Load project and compile
3. Run

### Contribution guidelines 

* Concurrent changes to the scene can and will cause irresolvable conflicts, if you believe a commit will violate the scene state lodge a pull request
* Tests should be written for C# in MS Visual Studio using Unit Tests
* Code review is done by Dylan and Dylan's code is reviewed by Tom
#### Code conventions
##### Unity Naming Conventions
* For scripts, name them in the format: ScriptName.cs
* For folders (and files), name them in the format: Folder Name
* For GameObjects, name them in the format of: Brain Model
* For functions, name them in the format of: HelloWorld()
* For variables, name them in the format of: thisVariable

#### Approval Conventions
Once a push has been made, @ djoh2593, and ask for a code review + optional testing to be done. Give thorough testing guidelines to test all functionality you have implemented.

TEMPLATE:

Testing required + Code Review required: @djoh2593

Testing Guidelines:

* Test that each brain element can be selected in the main scene with the left mouse button. When selected, the element should glow.

* Test that each brain element can be similarly deselected.


##### Directory heirarchy
Each asset type should have a corresponding folder within the project file. Once you create (or import) the asset, make sure to put it into the corresponding folder.
Additionally, make sure it is named in accordance to the naming conventions above.
### Who do I talk to? 

#### Product Owner
Ben Sand 
#### Tutor for Project Team 
Mauro Mello Jr 
#### Project Team 
* Michael Rizzuto - Team Leader 
* Dylan Johnston - Assistant Team Leader and QA Analyst
* Sindhu Harinath - Documentation Specialist
* Tom Ingram - Developer 

