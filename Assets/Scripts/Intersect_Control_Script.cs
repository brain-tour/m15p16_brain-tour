﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Intersect_Control_Script : MonoBehaviour {
	public Vector3 dir;
	public Slider slider;
	private Vector3 originalPos;
	// Use this for initialization
	void Start () {
		originalPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void updateIntersect() {
		transform.position = originalPos + dir * slider.value;
	}
}
