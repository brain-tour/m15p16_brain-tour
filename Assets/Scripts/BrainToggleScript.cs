﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BrainToggleScript : MonoBehaviour {
	Toggle toggle;
	GameObject toggleTarget;

	// Use this for initialization
	void Start () {
		toggle = gameObject.GetComponent<Toggle>();
		toggle.onValueChanged.AddListener(OnClick);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void clickMenu(bool clicked) {
		toggleTarget.SetActive(clicked);
	}

	void OnClick(bool clicked) {
		toggleTarget.SetActive(clicked);
	}

	public void SetTarget(GameObject toggleTarget) {
		this.toggleTarget = toggleTarget;
	}

	public void SetText(string text) {
		transform.GetChild(1).GetComponent<Text>().text = text;
	}
}
