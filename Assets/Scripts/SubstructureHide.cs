﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubstructureHide : MonoBehaviour {

	// Use this for initialization
	void Start () {
        //Experimentation with disabling parts of the brain based on their mesh name
        Renderer[] renderers = gameObject.GetComponentsInChildren<Renderer>();
        int i = 0;
        foreach (Renderer r in renderers)
        {
            if (r.name.Equals("Veins"))
            {
                Debug.Log("LOCATED VEINS");
                //keep veins turned on so dont disable its renderer
                continue;
            }
            //if not specified name, disable the mesh
            r.enabled = false;
            i++;
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
