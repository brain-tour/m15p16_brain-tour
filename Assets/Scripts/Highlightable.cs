using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Highlightable : MonoBehaviour
{
    public Material highlightMaterial;
    private Material normalMaterial;
    private bool highlighted = false;
    public bool selected = false;
    private MeshRenderer meshRenderer;

    // Use this for initialization
    void Start()
    {
        meshRenderer = gameObject.GetComponent<MeshRenderer>();
        normalMaterial = meshRenderer.material;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (!highlighted && !selected)
        {
            meshRenderer.material = normalMaterial;
        }

        /*if (selected && Input.GetMouseButtonDown(1))
        {
            gameObject.SetActive(false);
        }*/

        highlighted = false;
    }

    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(1))
        {
            gameObject.SetActive(false);
        }
        if (Input.GetMouseButtonDown(0))
        {
            SetSelected(!GetSelected());
        }

    }

    /*void OnMouseDown() {
		SetSelected(!GetSelected());
	}*/

    public void SetHighlighted(bool highlighted)
    {
        if (highlighted && !this.highlighted)
        {
            meshRenderer.material = highlightMaterial;
        }
        else if (!highlighted && this.highlighted)
        {
            meshRenderer.material = normalMaterial;
        }

        this.highlighted = highlighted;
    }

    public void SetSelected(bool selected)
    {
        this.selected = selected;
        SetHighlighted(selected);

        if (selected == true)
        {

        }
    }

    public bool GetSelected()
    {
        return selected;
    }

    public bool IsHighlighted()
    {
        return highlighted;
    }
}
