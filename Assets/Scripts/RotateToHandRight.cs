﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Valve.VR.InteractionSystem;
using VolumeViewer;
[RequireComponent( typeof( Interactable ) )]

public class RotateToHandRight : MonoBehaviour {
	[Tooltip( "The flags used to attach this object to the hand." )]
	public Hand.AttachmentFlags attachmentFlags = Hand.AttachmentFlags.ParentToHand | Hand.AttachmentFlags.DetachFromOtherHand;

	[Tooltip( "Name of the attachment transform under in the hand's hierarchy which the object should should snap to." )]
	private SteamVR_TrackedController _controller;
	public string attachmentPoint;
	public bool restoreOriginalParent = false;
	public bool grabbed = false;
	public Vector3 grabPosition;
	public float rotationSpeed;
	public Quaternion grabRotation;
	public UnityEvent onPickUp;
	public UnityEvent onDetachFromHand;
	public Transform brainTransform;
	public GameObject container;
	public GameObject container_ref;
	public Transform old_parent;
	public int mode = 0;
	public VolumeRenderer volumeRenderer;
	private ClickedEventArgs eArgs;
	// Use this for initialization
	void Start () {
		_controller = GetComponent<SteamVR_TrackedController>();
		_controller.TriggerClicked += TriggerClicked;
		_controller.TriggerUnclicked += TriggerUnclicked;
	}
	
	// Update is called once per frame
	void Update () {
		if (grabbed)
		{
		// Controls what happens when the object is 'grabbed' 
			Vector3 curPosition = transform.position;
			Vector3 offset = curPosition - grabPosition;
			Quaternion handRot = transform.rotation;

			Debug.Log("Hand position: " + offset);

			if (mode == 0)
			{
				float rotationDiff = handRot.z - grabRotation.z;
				Vector3 rotation = new Vector3();
				rotation.y = -(offset.x) * rotationSpeed;
				rotation.x = -(offset.y) * rotationSpeed;
				rotation.z = -rotationDiff * rotationSpeed * 0.3f;
				container_ref.transform.Rotate(rotation);
			}
			else if (mode == 1)
			{
				Debug.Log("x offset is " + offset.x + ", y offset is " + offset.y + ", z offset is " + offset.z);
				container_ref.transform.Translate(Vector3.forward * offset.z, Space.World);
				container_ref.transform.Translate(Vector3.right * offset.x, Space.World);
				container_ref.transform.Translate(Vector3.up * offset.y, Space.World);
			}
			else
			{
				container_ref.transform.localScale *=  1.0f - offset.z * 2.0f;
			}

			grabPosition = curPosition;
			grabRotation = handRot;
		}
	}

	//Controls what happens when the trigger is clicked. Allows for 'grabbing' and movement/rotation of the brain model. 
	private void TriggerClicked(object sender, ClickedEventArgs e)
	{
		Debug.Log("Clicked");
		if (!grabbed)
		{
			Collider[] hitColliders = Physics.OverlapSphere(transform.position, 0.2f);
			Transform grabTransform = null;
			float smallestDist = 10000.0f;

			foreach(Collider col in hitColliders)
			{
				if (col.gameObject.tag == "canGrabRightOnly")
				{
					if (grabTransform == null || Vector3.Distance(transform.position, col.transform.position) <= smallestDist)
					{
						grabTransform = col.transform;
						smallestDist = Vector3.Distance(transform.position, col.transform.position);
					}
				}
			}

			if (grabTransform != null)
			{
					container_ref = Instantiate(container, grabTransform.position, Quaternion.identity);
					container_ref.transform.LookAt(Camera.main.transform.position);
					old_parent = grabTransform.parent;
					grabTransform.SetParent(container_ref.transform);

					grabPosition = transform.position;
					grabRotation = transform.rotation;
					grabbed = true;
			}
			else
			{
				Debug.Log("Couldn't find anything to grab");
			}
		}
	}

	//Controls what happens when user stops clicking trigger, the object is no longer 'grabbed' and is left in its position 
	private void TriggerUnclicked(object sender, ClickedEventArgs e)
	{
		container_ref.transform.GetChild(0).SetParent(old_parent);
		Destroy(container_ref);
		grabbed = false;
	}
}
