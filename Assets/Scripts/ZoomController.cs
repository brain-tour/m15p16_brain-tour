﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoomController : MonoBehaviour {

    // Use this for initialization
    public float minFov = 15f;
    public float maxFov = 90f;
    public float sensitivity = 10f;

    // Update is called once per frame
    void Update () {
        //check the scrollwheel and ajust the FOV depending on direction of scroll
        float fov = Camera.main.fieldOfView;
        fov-= Input.GetAxis("Mouse ScrollWheel") * sensitivity;
        fov = Mathf.Clamp(fov, minFov, maxFov);
        Camera.main.fieldOfView = fov;

        


    }
}
