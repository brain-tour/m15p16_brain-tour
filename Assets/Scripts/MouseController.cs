﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MouseController : MonoBehaviour {
    private Slider rotateSpeedSlider;
    bool mouseDown = false;
    // Use this for initialization
    void Start () {
        rotateSpeedSlider = GameObject.FindGameObjectWithTag("RotateSpeedSlider").GetComponent<Slider>();

    }

    // Update is called once per frame
    void Update () {
        //read in the X and Y inputs
        float directionInputX = -Input.GetAxis("Mouse X");
        float directionInputY = Input.GetAxis("Mouse Y");

        //Mouse state resets each frame, store in a variable between frames
        if (Input.GetMouseButton(1))
        {
            transform.Rotate(Time.deltaTime * directionInputY * rotateSpeedSlider.value * 1.5f, Time.deltaTime * directionInputX * rotateSpeedSlider.value * 1.5f, 0, Space.World);
        }
    }
        
}
