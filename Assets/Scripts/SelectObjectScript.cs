﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectObjectScript : MonoBehaviour {
	Ray ray;
	RaycastHit hit;
	public bool enableHoverHighlight;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (enableHoverHighlight) {
			ray = Camera.main.ScreenPointToRay(Input.mousePosition);

			if (Physics.Raycast(ray, out hit)) {
				Highlightable highlightable = hit.collider.gameObject.GetComponent<Highlightable>();

				if (highlightable != null) {
					highlightable.SetHighlighted(true);
				}
			} 
		}
	}
}
