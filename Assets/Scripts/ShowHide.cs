﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowHide : MonoBehaviour {
	
	public GameObject togglePrefab;
	public int menuXStart = 0;
	public int menuYStart = 0;
	public int menuYOffset;
	
	// Use this for initialization
	void Start () {
		GameObject brain = GameObject.FindGameObjectWithTag("Brain");
		GameObject showHideMenu = GameObject.FindGameObjectWithTag("Show-Hide Menu");
		GameObject showHideButton = GameObject.FindGameObjectWithTag("Show-Hide Button");


		int brainComponentCount = brain.transform.childCount;

		for (int i = 0; i < brainComponentCount; i++) {
			GameObject brainPart = brain.transform.GetChild(i).gameObject;

			Debug.Log("Creating toggle for " + brainPart.name);
			
			GameObject toggle = Instantiate(togglePrefab, 
				new Vector3(menuXStart, menuYStart + i * menuYOffset, 0), 
				togglePrefab.transform.rotation) as GameObject;

			toggle.transform.SetParent(showHideMenu.transform, false);
			showHideButton.GetComponent<MenuToggle>().AddTarget(toggle);
			toggle.GetComponent<BrainToggleScript>().SetTarget(brainPart);
			toggle.GetComponent<BrainToggleScript>().SetText(brainPart.name);
		}		
	}
	
	// Update is called once per frame
	void Update () {

	}
}
