﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Valve.VR.InteractionSystem;
using VolumeViewer;
[RequireComponent( typeof( Interactable ) )]

/**
* Controls the behaviour of the left hand VR controller. 
* Specifically, it logs rotation when the controller trigger is clicked and controls what happens when the pad/joystick is used. 
*/

public class HandControl : MonoBehaviour {
	[Tooltip( "The flags used to attach this object to the hand." )]
	public Hand.AttachmentFlags attachmentFlags = Hand.AttachmentFlags.ParentToHand | Hand.AttachmentFlags.DetachFromOtherHand;

	[Tooltip( "Name of the attachment transform under in the hand's hierarchy which the object should should snap to." )]
	private SteamVR_TrackedController _controller;
	public string attachmentPoint;
	public bool restoreOriginalParent = false;
	public bool grabbed = false;
	public Vector3 grabPosition;
	public float rotationSpeed;
	public Quaternion grabRotation;
	public UnityEvent onPickUp;
	public UnityEvent onDetachFromHand;
	public Transform brainTransform;
	public Transform cullingTransform;
	public GameObject container;
	public GameObject container_ref;
	public Transform old_parent;
	public bool rightHand = false;
	public int mode = 0;
	public string customTagGrab;
	public VolumeRenderer volumeRenderer;
	private bool padTouched = false;
	private bool menuEnabled = false;
	private ClickedEventArgs eArgs;
	private UIScript UIController;
	

	// Use this for initialization
	void Start () {
		UIController = GetComponent<UIScript>();
		_controller = GetComponent<SteamVR_TrackedController>();
		_controller.PadClicked += PadClicked;
		_controller.PadTouched += PadTouched;
		_controller.PadUntouched += PadUntouched;
		_controller.MenuButtonClicked += MenuButtonClicked;
		_controller.TriggerClicked += TriggerClicked;
		_controller.TriggerUnclicked += TriggerUnclicked;
	}
	
	// Update is called once per frame
	void Update () {
		if (grabbed)
		{
		// Controls what happens when the trigger is clicked and the object is 'grabbed' 
			Vector3 curPosition = transform.position;
			Vector3 offset = curPosition - grabPosition;
			Quaternion handRot = transform.rotation;

			// Controls rotation of object if right menu item clicked 
			if (mode == 0)
			{
				float rotationDiff = handRot.z - grabRotation.z;
				Vector3 rotation = new Vector3();
				rotation.y = -(offset.x) * rotationSpeed * (1/brainTransform.localScale.y);
				rotation.x = -(offset.y) * rotationSpeed * (1/brainTransform.localScale.x);
				rotation.z = -rotationDiff * rotationSpeed * 0.3f;
				container_ref.transform.Rotate(rotation);
			}
			
			// Controls movement of object top menu item clicked 
			else if (mode == 1)
			{
				container_ref.transform.Translate(Vector3.forward * offset.z, Space.World);
				container_ref.transform.Translate(Vector3.right * offset.x, Space.World);
				container_ref.transform.Translate(Vector3.up * offset.y, Space.World);
			}
			
			// Controls scaling of object 
			else
			{
				container_ref.transform.localScale -=  new Vector3(offset.z, offset.z, offset.z);
			}

			grabPosition = curPosition;
			grabRotation = handRot;
		}

		//Controls the joystick/pad selection of menu items. References UIScript.cs for the selection of menu items in the UI. 
		if (padTouched)
		{
			Vector2 dir = new Vector2(eArgs.padX, eArgs.padY);
			Vector2 dirNormalized = dir.normalized;

			double radians = Mathf.Atan2(dirNormalized.y, -dirNormalized.x);
			double degrees = (radians / Mathf.PI) * 180.0f;

			degrees = degrees - 90;

			if (degrees < 0)
			{
				degrees += 360;
			}

			if (dir.magnitude > 0.9f)
			{
				if (degrees >= 315 || degrees < 45)
				{
					//The degrees covered map to the Top button in the controller menu 
					UIController.selectTop();
				}
				else if (degrees >= 45 && degrees < 135)
				{
					// These map to the Right button 
					UIController.selectRight();
				}
				else if (degrees >= 135 && degrees < 225)
				{
					// These map to the Bottom button 
					UIController.selectBottom();
				}
				else
				{
					// Otherwise the Left button is chosen 
					UIController.selectLeft();
				}
			}
			else
			{
				// If no option on the pad/joystick is chosen, then no menu item is selected
				UIController.deselect();
			}
		}
	}

	// Controls behaviour when top menu button clicked 
	public void UIUp()
	{
		if (!rightHand)
		{
			mode = 1;
		}
		else
		{
			cullingTransform.parent = brainTransform;
		}
	}

	// Controls behaviour when left menu button clicked 
	public void UILeft()
	{
		if (!rightHand)
		{
			mode = 2;
		}
		else
		{
			cullingTransform.localScale *=  1.005f;
		}
		
	}


	// Controls behaviour when right menu button clicked 
	public void UIRight()
	{
		if (!rightHand)
		{
			mode = 0;
		}
		else
		{
			cullingTransform.localScale *=  0.995f;
		}
	}

	//Controls behaviour when bottom menu button clicked 
	public void UIBottom()
	{
		if (!rightHand)
		{
		}
		else
		{
			cullingTransform.parent = transform;
			cullingTransform.localPosition = new Vector3(0, 0, 0);
		}
	}

	private void PadTouched(object sender, ClickedEventArgs e)
	{
		eArgs = e;
		padTouched = true;
		
	}

	private void PadUntouched(object sender, ClickedEventArgs e)
	{
		UIController.deselect();
		padTouched = false;
	}

	private void PadClicked(object sender, ClickedEventArgs e)
	{
		mode ++;
		if (mode > 2)
		{
			mode = 0;
		}


	}

	//References UIScript.cs and reveals all menu items around the controller 
	private void MenuButtonClicked(object sender, ClickedEventArgs e)
	{
		UIController.toggleMenu();
		menuEnabled = !menuEnabled;
	}

	//Controls what happens when the trigger is clicked. Allows for 'grabbing' and movement/rotation of the brain model. 
	private void TriggerClicked(object sender, ClickedEventArgs e)
	{
		Debug.Log("Clicked");
		if (!grabbed)
		{
			Collider[] hitColliders = Physics.OverlapSphere(transform.position, 0.2f);
			Transform grabTransform = null;
			float smallestDist = 10000.0f;

			foreach(Collider col in hitColliders)
			{
				if (col.gameObject.tag == "canGrab" || col.gameObject.tag == customTagGrab)
				{
					if (grabTransform == null || Vector3.Distance(transform.position, col.transform.position) <= smallestDist)
					{
						grabTransform = col.transform;
						smallestDist = Vector3.Distance(transform.position, col.transform.position);
					}
				}
			}

			if (grabTransform != null)
			{
					container_ref = Instantiate(container, grabTransform.position, Quaternion.identity);
					container_ref.transform.LookAt(Camera.main.transform.position);
					old_parent = grabTransform.parent;
					grabTransform.SetParent(container_ref.transform);

					grabPosition = transform.position;
					grabRotation = transform.rotation;
					grabbed = true;
			}
			else
			{
				Debug.Log("Couldn't find anything to grab");
			}
		}
	}

	//Controls what happens when user stops clicking trigger, the object is no longer 'grabbed' and is left in its position 
	private void TriggerUnclicked(object sender, ClickedEventArgs e)
	{
		container_ref.transform.GetChild(0).SetParent(old_parent);
		Destroy(container_ref);
		grabbed = false;
	}
}
