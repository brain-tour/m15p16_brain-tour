﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class PanScript : MonoBehaviour {
	public float panSpeed;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		float mouseX = -Input.GetAxis("Mouse X");
		float mouseY = -Input.GetAxis("Mouse Y");
		UnityEngine.Rect screenBounds = new UnityEngine.Rect(0,0, Screen.width, Screen.height);

		if (Input.GetMouseButton(0) && !EventSystem.current.IsPointerOverGameObject() && screenBounds.Contains(Input.mousePosition)) {
			transform.Translate(new Vector3(mouseX * panSpeed, mouseY * panSpeed, 0));
		}
	}
}
