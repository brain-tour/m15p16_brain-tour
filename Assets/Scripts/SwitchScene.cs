﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SwitchScene : MonoBehaviour {

	void onClick()
    {
        Scene currentScene = SceneManager.GetActiveScene();

        if (currentScene.name == "mainscene")

            Application.LoadLevel("MRI");

        else
            Application.LoadLevel("mainscene");
    }

    void Update()
    {
        if (Input.GetKeyDown("["))
            onClick();
    }
}
