﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleGradual : MonoBehaviour {

    /*
    These keep track of the base values of the object
    */

    public int startSize = 1;
    public int minSize = 1;
    public int maxSize = 2;

    /* Speed of gradual change*/
    public float speed = 1.0f;

  
    private Vector3 endScale;
    private Vector3 startScale;
    private float current;

    // Use this for initialization
    void Start () {
        startScale = transform.localScale;
        transform.localScale = startScale * startSize;
        current = startSize;
        endScale = startScale * startSize;

	}
	
	// Update is called once per frame
	void Update () {
            
        transform.localScale = Vector3.Lerp(transform.localScale, endScale, speed * Time.deltaTime);
	}

    /*
    Either set current scale up or down
    */
    public void ScaleObject(int sign)
    {
        current += 0.1f * sign;

        current = Mathf.Clamp(current, minSize, maxSize+1);

        endScale = current * startScale;
    }
}
