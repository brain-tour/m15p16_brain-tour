﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(VolumeViewer.VolumeComponent))]

public class MRIcolour : MonoBehaviour {
    public int test = 0;
    VolumeViewer.VolumeComponent volumeComponent;

    // Use this for initialization
    void Start () {
        volumeComponent = GetComponent<VolumeViewer.VolumeComponent>();
    }

    public void ChangeSettings()
    {
        volumeComponent.maxSamples = 256;
        volumeComponent.cutValueRangeMin = 0.07f;
        volumeComponent.cutValueRangeMax = 1f;
        volumeComponent.valueRangeMin = 0f;
        volumeComponent.valueRangeMax = 0.4f;
        volumeComponent.opacity = 0.05f;
        volumeComponent.tfDataBlendModeAsInt = 2;
        volumeComponent.overlayBlendModeAsInt = 3;
        volumeComponent.tfOverlayBlendModeAsInt = 1;
        
    }
    // Update is called once per frame
    void Update () {

    }
}
