﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuToggle : MonoBehaviour {
    public Button toggleButton;
    public List<GameObject> elements;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    /*void OnClick()
    {
        foreach(GameObject element in elements)
        {
            
            if (element == null) continue;
            Debug.Log(element.name);
            element.SetActive(!element.activeSelf);
        }
    } */

    public void clickMenu() {
    	Debug.Log("Clicked!");
    	foreach(GameObject element in elements)
        {
            
            if (element == null) continue;
            Debug.Log(element.name);
            element.SetActive(!element.activeSelf);
        }
    }

    public void AddTarget(GameObject target) {
        elements.Add(target);
    }

    public void SetText(string text) {
        transform.GetChild(0).GetComponent<Text>().text = text;
    }
}
