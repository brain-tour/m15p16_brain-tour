﻿using UnityEngine;
using UnityEngine.UI;

public class ButtonControlledRotation : MonoBehaviour {
    private int direction_hor = 0;
    private int direction_ver = 0;
    private int direction_spin = 0;
    private bool reset = false;
    private Slider rotateSpeedSlider;

    void Start()
    {
        // Finds the slider for the rotation speed. The slider must be tagged with the tag "RotateSpeedSlider".
        rotateSpeedSlider = GameObject.FindGameObjectWithTag("RotateSpeedSlider").GetComponent<Slider>();
    }

    void Update (){

        transform.Rotate (Time.deltaTime * direction_ver * rotateSpeedSlider.value, 0, 0, Space.World);
        transform.Rotate (0, 0, Time.deltaTime * direction_spin * -rotateSpeedSlider.value, Space.World);
        transform.Rotate (0, Time.deltaTime * direction_hor * rotateSpeedSlider.value, 0, Space.World);

        if (reset) {
			transform.rotation = Quaternion.identity;
			transform.Rotate (-90, 0, 0);
		}
	}

	//Utility function used to begin model rotation. Encoded as:
	public void press(int direction_input){
        switch(direction_input)
        {
            case -1: reset = true;
                break;
            case 1: direction_hor = -1;
                break;
            case 2: direction_hor = 1;
                break;
            case 3: direction_ver = -1;
                break;
            case 4: direction_ver = 1;
                break;
            case 5: direction_spin = -1;
                break;
            case 6: direction_spin = 1;
                break;
        }
	}

	//on mouse release rotation stops and direction reset
	public void release(){
        direction_hor = 0;
        direction_ver = 0;
        direction_spin = 0;
        reset = false;
	}
}
