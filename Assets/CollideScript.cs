using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollideScript : MonoBehaviour {
	bool enabledHit = false;
	public GameObject enableObject;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		int layerId = 9;
 		int layerMask = 1 << layerId;
		Collider[] hits = Physics.OverlapBox(transform.position, 
			new Vector3(transform.localScale.x * 0.4f, transform.localScale.y * 0.4f, transform.localScale.z * 0.4f), 
			transform.rotation, 
			layerMask);

		if (hits.Length != 0 && !enabledHit)
		{
			Debug.Log("Setting active!");
			enableObject.SetActive(true);
			enabledHit = true;
		}
		else if (hits.Length == 0 && enabledHit)
		{
			Debug.Log("Setting inactive");
			enableObject.SetActive(false);
			enabledHit = false;
		}
	}	
}
