﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetPosition : MonoBehaviour {
	public Transform followTransform;
	public bool followObject = true;
	public bool scaleWithObject;
	public Vector3 initialScale;
	public Vector3 initialPosition;
	public Vector3 offset;
	// Use this for initialization
	void Start () {
		initialScale = followTransform.localScale;
		initialPosition = followTransform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (followObject)
		{
			transform.position = followTransform.position + offset;
		}

		if (scaleWithObject)
		{
			Vector3 differenceScale = followTransform.localScale - initialScale;
			transform.localScale += differenceScale;
			initialScale = followTransform.localScale;
		}
	}
}
