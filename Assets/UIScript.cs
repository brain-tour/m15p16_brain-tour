﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/**
* Controls what happens to the UI when each button around the controller is pressed 
*/

public class UIScript : MonoBehaviour {
	public Image top;
	public Image bottom;
	public Image left;
	public Image right;
	public Color selectColor;
	public Color deselectColor;
	public float initialScale;
	public float transformSlide;
	public float imageScale;
	public int selected = 0;
	public float scaleIncrement;
	private HandControl handController;
	public bool staySelected;
	
	// Use this for initialization
	void Start () {
		top.transform.position -= new Vector3(0, transformSlide, 0);
		right.transform.position -= new Vector3(-transformSlide, 0, 0);
		left.transform.position -= new Vector3(transformSlide, 0, 0);
		bottom.transform.position -= new Vector3(0, -transformSlide, 0);

		top.transform.localScale = new Vector3(imageScale, imageScale, imageScale);
		bottom.transform.localScale = new Vector3(imageScale, imageScale, imageScale);
		right.transform.localScale = new Vector3(imageScale, imageScale, imageScale);
		left.transform.localScale = new Vector3(imageScale, imageScale, imageScale);
		handController = GetComponent<HandControl>();
		initialScale = top.transform.localScale.x;
	}
	
	
	/**
	* Update is called once per frame
	* 
	* When each button is selected, it will change colour and increase in size to let the user know that it has been selected. 
	* All other buttons need to remain in their original, deselected state 
	*/
	void Update () {
		if (selected == 0)
		{
			//When NO button is selected 
			top.color = deselectColor;
			left.color = deselectColor;
			right.color = deselectColor;
			bottom.color = deselectColor;
			top.transform.localScale = new Vector3(initialScale, initialScale, initialScale);
			bottom.transform.localScale = new Vector3(initialScale, initialScale, initialScale);
			right.transform.localScale = new Vector3(initialScale, initialScale, initialScale);
			left.transform.localScale = new Vector3(initialScale, initialScale, initialScale);
		}
		else if (selected == 1)
		{
			//When the TOP button is selected 
			left.color = deselectColor;
			right.color = deselectColor;
			bottom.color = deselectColor;
			top.color = selectColor;
			top.transform.localScale = new Vector3(initialScale, initialScale, initialScale) + new Vector3(scaleIncrement, scaleIncrement, scaleIncrement);
			bottom.transform.localScale = new Vector3(initialScale, initialScale, initialScale);
			right.transform.localScale = new Vector3(initialScale, initialScale, initialScale);
			left.transform.localScale = new Vector3(initialScale, initialScale, initialScale);

			handController.UIUp();
		}
		else if (selected == 2)
		{
			//When the RIGHT button is selected
			handController.UIRight();
			right.color = selectColor;
			top.color = deselectColor;
			left.color = deselectColor;
			bottom.color = deselectColor;
			right.transform.localScale = new Vector3(initialScale, initialScale, initialScale) + new Vector3(scaleIncrement, scaleIncrement, scaleIncrement);
			top.transform.localScale = new Vector3(initialScale, initialScale, initialScale);
			bottom.transform.localScale = new Vector3(initialScale, initialScale, initialScale);
			left.transform.localScale = new Vector3(initialScale, initialScale, initialScale);
		}
		else if (selected == 3)
		{
			//When the BOTTOM button is selected
			handController.UIBottom();
			bottom.color = selectColor;
			top.color = deselectColor;
			left.color = deselectColor;
			right.color = deselectColor;
			bottom.transform.localScale = new Vector3(initialScale, initialScale, initialScale) + new Vector3(scaleIncrement, scaleIncrement, scaleIncrement);
			top.transform.localScale = new Vector3(initialScale, initialScale, initialScale);
			right.transform.localScale = new Vector3(initialScale, initialScale, initialScale);
			left.transform.localScale = new Vector3(initialScale, initialScale, initialScale);
		}
		else
		{
			//When the LEFT button is selected
			handController.UILeft();
			left.color = selectColor;
			top.color = deselectColor;
			right.color = deselectColor;
			bottom.color = deselectColor;
			left.transform.localScale = new Vector3(initialScale, initialScale, initialScale) + new Vector3(scaleIncrement, scaleIncrement, scaleIncrement);
			top.transform.localScale = new Vector3(initialScale, initialScale, initialScale);
			bottom.transform.localScale = new Vector3(initialScale, initialScale, initialScale);
			right.transform.localScale = new Vector3(initialScale, initialScale, initialScale);
		}
	}

	
	//The following are referenced by HandControl.cs which controls the behaviour of the left controller
	public void toggleMenu()
	{
		top.enabled = !top.enabled;
		bottom.enabled = !bottom.enabled;
		left.enabled = !left.enabled;
		right.enabled = !right.enabled;
	}

	public void selectTop()
	{
		selected = 1;
		top.color = selectColor;
	}

	public void selectBottom()
	{
		selected = 3;
		bottom.color = selectColor;
	}

	public void selectLeft()
	{
		selected = 4;
		left.color = selectColor;
	}

	public void selectRight()
	{
		selected = 2;
		right.color = selectColor;
	}

	public void deselect()
	{
		if (!staySelected)
			selected = 0;
	}
}
